﻿var lastElement;
var lastElementStyle;
var isMatched = false;
var userSession = $.session.get("username");
var idSession = $.session.get("Id");
var isOldPassword = false;
var roleSession = $.session.get("role");
$(document).ready(function () {
    $('#userListTable').DataTable();
    $('#welcome').text(userSession);
    $('#newPassword, #cnfrmPwd').change(function () {
        if ($('#cnfrmPwd').val()) {
            if ($('#newPassword').val() == $('#cnfrmPwd').val()) {
                $('#cnfrmPwd').css('background-color', '#4dd2ff');
                $('#newPassword').css('background-color', '#4dd2ff');
                isMatched = true;
            } else {
                $('#cnfrmPwd').css('background-color', '#ff8080');
                $('#newPassword').css('background-color', '#ff8080');
                isMatched = false;
            }
        } else {
            if ($('#newPassword').val() == "") {
                $('#cnfrmPwd').css('background-color', '#ffffff');
                $('#newPassword').css('background-color', '#ffffff');
                isMatched = false;
            }
        }
    });
});
// View Page
//----------------------------------------------
//----------------------------------------------



if (!roleSession) {
    alert('Access Denied. Please login');
    window.location.href = _spPageContextInfo.webAbsoluteUrl + "/Pages/Default.aspx";
} else {
    document.getElementById('DeltaPlaceHolderPageTitleInTitleArea').innerHTML = '';
}

//---------------------------AngularJS---------------------------



//---------------------------REST API---------------------------

//Get list of item from list.
function getList() {
    var listUser;
    $.ajax({
        url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('UserList')/items",
        type: "GET",
        headers: { "accept": "application/json;odata=verbose" },
        success: function (data) {
            listUser = data.d.results;
        },
        error: function (xhr) {
            alert(xhr.status + ': ' + xhr.statusText);
        },
        async: false
    });
    return listUser;
}

function updatePassword(id, password) {
    $.ajax({
        url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/GetByTitle('UserList')/items(" + id + ")",
        type: "POST",
        contentType: "application/json;odata=verbose",
        data: JSON.stringify
            ({
                __metadata:
                    {
                        type: "SP.Data.UserListListItem"
                    },
                Password: password,
            }),
        headers: {
            "accept": "application/json;odata=verbose",
            "Content-Type": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
            "X-HTTP-Method": "MERGE",
            "If-Match": "*"
        },
        success: function (data) {
            alert('Update Password Susscess!');
        },
        error: function (xhr) {
        },
        async: false
    });
}

function deleteItem(id) {
    $.ajax({
        url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/GetByTitle('UserList')/items(" + id + ")",
        method: "POST",
        headers: {
            "Accept": "application/json; odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
            "If-Match": "*",
            "X-HTTP-Method": "DELETE"
        },
        success: function () {
            alert('item deleted');
        },
        error: function (xhr) {
        }
    });
}

function banUser(id, isBanned) {
    $.ajax({
        url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/GetByTitle('UserList')/items(" + id + ")",
        type: "POST",
        contentType: "application/json;odata=verbose",
        data: JSON.stringify
            ({
                __metadata:
                    {
                        type: "SP.Data.UserListListItem"
                    },
                isBanned: !isBanned,
            }),
        headers: {
            "accept": "application/json;odata=verbose",
            "Content-Type": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
            "X-HTTP-Method": "MERGE",
            "If-Match": "*"
        },
        success: function (data) {
            alert('User is ' + (isBanned == true ? "unbanned" : "banned"));
        },
        error: function (xhr) {
        },
        async: false
    });
}

//Compare current input password on screen and current password in list.
function checkOldPassword(username, password) {
    $.ajax({
        url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('UserList')/items?$select=Username,Password&$filter=(Username eq'" + username + "')and(Password eq '" + password + "')",
        type: "GET",
        headers: { "accept": "application/json;odata=verbose" },
        async: false,
        success: function (data) {
            if (data.d.results.length == 1) {
                isOldPassword = true;
            } else {
                isOldPassword = false;
                alert("Wrong Old Password");
            }
        }, eror: function (data) {

        }
    });
}

function updateItemAPI(id, address, description, gender) {
    $.ajax({
        url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/GetByTitle('UserList')/items(" + id + ")",
        type: "POST",
        contentType: "application/json;odata=verbose",
        data: JSON.stringify
            ({
                __metadata:
                    {
                        type: "SP.Data.UserListListItem"
                    },
                Address: address,
                Description1: description,
                Gender1: gender,
            }),
        headers: {
            "accept": "application/json;odata=verbose",
            "Content-Type": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
            "X-HTTP-Method": "MERGE",
            "If-Match": "*"
        },
        success: function (data) {
            alert('Update Susscess!');
        },
        error: function (xhr) {
        },
        async: false
    });
}

//---------------------------Normal Script---------------------------
function logout() {
    $.session.clear();
    window.location.href = _spPageContextInfo.webAbsoluteUrl + "/Pages/Default.aspx";
}