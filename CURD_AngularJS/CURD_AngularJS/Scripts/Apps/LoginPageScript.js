﻿ExecuteOrDelayUntilScriptLoaded(initializePage, "sp.js");
//var registerPageUrl = _spPageContextInfo.webAbsoluteUrl + "/Pages/CreatePage.aspx";

function initializePage() {
    //If user press enter on textbox do login().
    $(document).ready(function () {
        $("#txtUsername,#txtPassword").bind('keyup', function (event) {
            event.preventDefault();
            if (event.keyCode === 13) {
                document.getElementById("btnLogin").click();
            };
        })

    });

    //Change title text.
    document.getElementById('DeltaPlaceHolderPageTitleInTitleArea').innerHTML = 'Login';

}

function login(username, password) {
    var isInValid = 0;
    jQuery.ajax({
        url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('UserList')/items?$select=Id,Username,Password,isBanned,Role1&$filter=Username eq" +
            "'" + username + "'" + "and Password eq" + "'" + password + "'" + "",
        headers: { Accept: "application/json;odata=verbose" },
        async: false,
        success: function (data) {
            if (data.d.results.length == 0) {
                //alert("Invalid Username or Password. Please try again!!!");
                isInValid = 0;
            } else if (!data.d.results[0].isBanned) {
                $.session.set("username", data.d.results[0].Username);
                $.session.set("role", data.d.results[0].Role1);
                $.session.set("Id", data.d.results[0].Id);
                //window.location.href = _spPageContextInfo.webAbsoluteUrl + "/Pages/ViewPage.aspx"; 
                isInValid = 1;// login success
            } else {
                isInValid = 2; //account is banned         
            }
        }, eror: function () {
            alert("Error occurred...");
        }
    });
    return isInValid;
}



//---------------------------Normal Script---------------------------
