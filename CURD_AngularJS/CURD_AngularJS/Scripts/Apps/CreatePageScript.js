﻿ExecuteOrDelayUntilScriptLoaded(initializePage, "sp.js");

function initializePage() {
    // Compare password and confirm password on user input.
    $('#password, #cnfrmPwd').change(function () {
        if ($('#cnfrmPwd').val()) {
            if ($('#password').val() == $('#cnfrmPwd').val()) {
                $('#cnfrmPwd').css('background-color', '#4dd2ff');
                $('#password').css('background-color', '#4dd2ff');
                isMatched = true;
            } else {
                $('#cnfrmPwd').css('background-color', '#ff8080');
                $('#password').css('background-color', '#ff8080');
                isMatched = false;
            }
        } else {
            if ($('#password').val() == "") {
                $('#cnfrmPwd').css('background-color', '#ffffff');
                $('#password').css('background-color', '#ffffff');
                isMatched = false;
            }
        }
    });

    //Change title text.
    document.getElementById('DeltaPlaceHolderPageTitleInTitleArea').innerHTML = 'Create new account';
}

// Create Page
//----------------------------------------------
//----------------------------------------------
isExisted = false;
isMatched = false;
//---------------------------AngularJS---------------------------
//---------------------------REST API---------------------------
function addNewUser(username, password, address, description, gender) {
    var pageUrl = _spPageContextInfo.webAbsoluteUrl;
    var data = {
        __metadata: { 'type': 'SP.Data.UserListListItem' },
        Username: username,
        Password: password,
        Address: address,
        Role1: 1,
        isBanned: false,
        Description1: description,
        Gender1: gender.value
    };
    $.ajax({
        headers: {
            "Accept": "application/json; odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
            "If-Match": "*",
            "content-type": "application/json;odata=verbose"
        },
        url: pageUrl + "/_api/web/lists/getbytitle('UserList')/items",
        method: "POST",
        data: JSON.stringify(data),
        success: function (data) {
            showPopupDialog('User added successfully');
            window.location.href = _spPageContextInfo.webAbsoluteUrl + "/Pages/Default.aspx"
        },
        error: function (error) {
            showPopupDialog("Error: " + JSON.stringify(error));
        }
    });
}

//Check if username is exist in database.
function checkExist(username) {
    var Ownurl = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('UserList')/items?$select=Username&$filter=Username eq" + "'" + username + "'";
    jQuery.ajax({
        url: Ownurl,
        headers: { Accept: "application/json;odata=verbose" },
        async: false,
        success: function (data) {
            if (data.d.results.length == 0) {
                isExisted = true;
                changeInputColor("#4dd2ff");
            } else {
                isExisted = false;
                changeInputColor("#ff8080");
                showPopupDialog("Username is existed");
            }
        }, eror: function (error) {

        }
    });
}

//---------------------------Normal Script---------------------------

//Change background color of textbox to confirm user input.
function changeInputColor(color) {
    $("#username").css("background-color", color);
}

function showPopupDialog(errorMessage) {
    alert(errorMessage);
}

//Check if all field is valid to add new user.
function checkValid() {
    var isValid = false;
    var username = document.getElementById('username');
    var password = document.getElementById('password');
    var confirmPwd = document.getElementById('cnfrmPwd');
    var address = document.getElementById('address');
    var description = document.getElementById('description');
    var gender = document.getElementById('gender');
    if (username.value || password.value || password.value || confirmPwd.value || address.value || description.value || gender.value != "?") {
        if (isMatched && isExisted) {
            return true;
        }
    }
    return isValid;
}
