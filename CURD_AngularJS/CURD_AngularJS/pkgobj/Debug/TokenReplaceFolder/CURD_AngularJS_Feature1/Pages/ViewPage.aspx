﻿<%@ Page Language="C#" MasterPageFile="~masterurl/default.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <SharePoint:ScriptLink Name="sp.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />

    <!-- Add your JavaScript to the following file -->
    <script type="text/javascript" src="../Scripts/lib/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../Scripts/lib/jquery.session.js"></script>
    <script type="text/javascript" src="../Scripts/lib/angular.min.js"></script>
    <script type="text/javascript" src="../Scripts/lib/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../Scripts/popper.min.js"></script>
    <script type="text/javascript" src="../Scripts/lib/bootstrap.min.js"></script>
    <script type="text/javascript" src="../Scripts/Apps/ViewPageScript.js"></script>

        <!-- Add your CSS styles to the following file -->
    <link rel="Stylesheet" type="text/css" href="../Content/ViewPage.css" />
    <link rel="Stylesheet" type="text/css" href="../Content/bootstrap.min.css" />
    <link rel="Stylesheet" type="text/css" href="../Content/jquery.dataTables.css" />

</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <div ng-app="viewApp" ng-controller="myCtrl">
        <div style="padding-bottom: 80px;">
            <div class="content-left">
                Welcome,
                <label style="color: #0078d7" for="" id="welcome"></label>
            </div>
            <div style="float: right">
                <button type="button" ng-click="changePasswordPopup()" class="btn btn-outline-danger changePassword">Change Password</button>
                <button type="button" onclick="logout()" class="btn btn-outline-danger changePassword">Log Out</button>
            </div>
        </div>

        <input type="text" name="someData" ng-model="isAdmin" style="display: none;" />
        <table class="table table-striped" id="userListTable">

            <thead class="table-thead">
                <tr>
                    <th width="100px">No.</th>
                    <th width="300px">Username</th>
                    <!-- <td>Password</td> -->
                    <!-- <td>Role</td> -->
                    <!-- <td>Description</td> -->
                    <!-- <td ng-hide="isAdmin">Gender</td> -->
                    <th width="500px">Address</th>
                    <th width="200px">Status</th>
                    <th width="280px" ng-hide="!isAdmin">Action</th>

                </tr>
            </thead>
            <tr ng-repeat="user in userList" style="height: 50px" ng-click="changeBgCol($event)">
                <td>{{$index + 1}}</td>
                <td ng-if="user.Username == isUser || isAdmin">
                    <a ng-click="getCurrentRow(user)">{{user.Username}}</a>
                </td>
                <td ng-if="user.Username != isUser && !isAdmin">{{user.Username}}
                </td>
                <td>{{user.Address}}</td>
                <td>{{user.isBanned == true ? 'Banned' : 'Active'}}</td>
                <td ng-hide="!isAdmin">
                    <button type="button" ng-hide="user.Username == isUser" ng-click="deleteUser(user.Id)" class="btn btn-danger">DELETE</button>
                    <button type="button" ng-hide="user.Username == isUser" ng-click="banUser(user.Id, user.isBanned)" class="btn btn-warning">{{user.isBanned == false ? 'BAN' : 'UNBAN'}}</button>
                </td>
            </tr>
        </table>
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">User Infomation</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="table-input">
                                <table class="table table-striped">
                                    <tr>
                                        <td>
                                            <label for="username">Username</label>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" id="currentUserName" disabled />
                                            <input type="hidden" id="currentId" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="address">Address</label>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" id="currentAddress" placeholder="Address" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="description">Description</label>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" id="currentDescription" placeholder="Description" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="gender">Gender</label>
                                        </td>
                                        <td>
                                            <select id="currentGender" class="form-control">
                                                <option value="{{x.value}}" ng-repeat="x in genders">{{x.name}}</option>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                                <button id="btnUpdate" class="btnPopup btn btn-outline-primary" type="button" ng-click="updateUser(user)">Update</button>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="modal-footer">
                    <button type="button" class="btn-outline-danger" data-dismiss="modal">Close</button>
                </div> -->
                </div>
            </div>
        </div>

        <!-- Modal Change Password -->
        <div class="modal fade" id="changePasswordModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Change Password</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="table-input">
                                <table class="table table-striped">
                                    <tr>
                                        <td>
                                            <label for="oldPassword">Old Password</label>
                                        </td>
                                        <td>
                                            <input type="password" ng-model="oldPassword" class="form-control" id="oldPassword" placeholder="Old Password" ng-blur="checkOldPassword()"
                                                minlength="6" maxlength="32" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="newPassword">New Password</label>
                                        </td>
                                        <td>
                                            <input type="password" ng-model="newPassword" class="form-control" id="newPassword" placeholder="New Password" minlength="6"
                                                maxlength="32" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="confirmPwd">Confirm Password</label>
                                        </td>
                                        <td>
                                            <input type="password" ng-model="confirmPwd" class="form-control" id="confirmPwd" placeholder="Confirm Password" minlength="6"
                                                maxlength="32" />
                                        </td>
                                    </tr>

                                </table>
                                <button id="btnSubmit" type="button" ng-click="changePassword()" class="btn btn-outline-primary btnPopup">Submit</button>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn-outline-danger" data-dismiss="modal">Close</button>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <script>
        var viewApp = angular.module("viewApp", []);
        viewApp.controller("myCtrl", function ($scope) {

            //render table view
            $scope.userList = getList();

            //check if login user is admin
            $scope.isAdmin = roleSession == 0 ? true : false;

            $scope.genders = [{
                "value": 1,
                "name": "Male"
            }, {
                "value": 2,
                "name": "Female"
            }, {
                "value": 0,
                "name": "Unknown"
            }];

            //get current login user.
            $scope.isUser = userSession;

            //show modal "update"
            $scope.getCurrentRow = function (user) {
                $("#currentUserName").val(user.Username);
                $("#currentId").val(user.Id);
                $("#currentAddress").val(user.Address);
                $("#currentDescription").val(user.Description1);
                $("#currentGender").val(user.Gender1);
                $("#myModal").modal({
                    "backdrop": "static",
                    "show": true
                });

            };

            $scope.updateUser = function () {
                var id = $("#currentId").val();
                var address = $("#currentAddress").val();
                var description = $("#currentDescription").val();
                var gender = $("#currentGender").val();
                updateItemAPI(id, address, description, gender);
                window.location.href = _spPageContextInfo.webAbsoluteUrl + "/Pages/ViewPage.aspx";
            };

            $scope.deleteUser = function (id) {
                deleteItem(id);
                window.location.href = _spPageContextInfo.webAbsoluteUrl + "/Pages/ViewPage.aspx";
            };

            $scope.banUser = function (id, isBanned) {
                banUser(id, isBanned);
                window.location.href = _spPageContextInfo.webAbsoluteUrl + "/Pages/ViewPage.aspx";
            };

            $scope.changePasswordPopup = function () {
                $("#changePasswordModal").modal({
                    "backdrop": "static",
                    "show": true
                })
            };

            //Confirm old password is matched on user input.
            $scope.checkOldPassword = function () {
                var oldPassword = document.getElementById('oldPassword');
                if (oldPassword.value) {
                    checkOldPassword(userSession, oldPassword.value);
                }
            };

            $scope.changePassword = function () {
                var newPassword = document.getElementById('newPassword');
                var confirmPwd = document.getElementById('confirmPwd');
                if (newPassword.value && confirmPwd.value) {
                    if (newPassword.value == confirmPwd.value) {
                        if (isOldPassword) {
                            updatePassword(idSession, newPassword.value);
                            window.location.href = _spPageContextInfo.webAbsoluteUrl + "/Pages/ViewPage.aspx";
                        } else {
                            alert("Wrong old password")
                        }
                    } else {
                        alert('Your password is not match');
                    }
                } else {
                    alert('Please fill all the blanks')
                }
            };

            //Change <tr> color on user click.
            $scope.changeBgCol = function ($event) {
                var ahihi = $event.currentTarget;
                if (lastElement) {
                    lastElement.style.backgroundColor = lastElementStyle;
                }
                lastElementStyle = ahihi.style.backgroundColor;
                if (lastElement != ahihi) {
                    ahihi.style.backgroundColor = "#66b3ff";
                    lastElement = ahihi;
                } else {
                    lastElement = null;
                }
            }
        });
    </script>
</asp:Content>
