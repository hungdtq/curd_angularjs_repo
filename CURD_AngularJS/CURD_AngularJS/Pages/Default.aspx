﻿<%-- The following 4 lines are ASP.NET directives needed when using SharePoint components --%>

<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%-- The markup and script in the following Content element will be placed in the <head> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <%--<script type="text/javascript" src="../Scripts/jquery-1.9.1.min.js"></script>--%>
    <SharePoint:ScriptLink Name="sp.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />
    <meta name="WebPartPageExpansion" content="full" />

    <!-- Add your CSS styles to the following file -->
    <link rel="Stylesheet" type="text/css" href="../Content/LoginPage.css" />
    <link rel="Stylesheet" type="text/css" href="../Content/bootstrap.min.css" />
    <link rel="Stylesheet" type="text/css" href="../Content/angular-material.min.css" />

    <!-- Add your JavaScript to the following file -->
    <script type="text/javascript" src="../Scripts/lib/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../Scripts/lib/jquery.session.js"></script>

</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Page Title
</asp:Content>

<%-- The markup and script in the following Content element will be placed in the <body> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">

    <!--<div class="login-template container" ng-app="LoginApps" ng-controller="AppsControl">
        <div class="template-header">
            LOG IN
        </div>
        <div class="line line-txtUsername">
            <div class="contain-txt">
                <span>Username:</span>
                <br>
                <input id="txtUsername" type="text" ng-model="user.username" placeholder="username..." />
            </div>
        </div>
        <div class="line line-txtPassword">
            <div class="contain-txt">
                <span>Password:</span>
                <br>
                <input id="txtPassword" type="password" ng-model="user.password" placeholder="password..." />
            </div>
        </div>
        <div class="btn-Login container" id="btnLogin" ng-click="login(user)" onkeypress="handleEnter(event)">
            <div class="txt-Login">
                <span>Sign In</span>
            </div>
        </div>
        <div class="txt-createUser">
            <span id="txt-Signup" ng-click="register()">Click here to Sign Up</span>
        </div>
    </div> -->


    <div ng-app="LoginApps">
        <md-content ng-controller="AppsControl" layout="row" layout-align="space-around" layout-padding="layout-padding" class="login-form">
            <md-card flex="flex" flex-gt-sm="50" flex-gt-md="33">
                <md-toolbar>
                    <div class="md-toolbar-tools header">
                        <h2>
                            <span>Login Form</span>
                        </h2>
                    </div>
                </md-toolbar>
                <md-card-content>
                    <form name="Form">
                        <md-input-container class="md-block">
                            <label class="txtLabel">Username:</label>
                            <input type="text" id="txtUsername" required name="email" ng-model="user.username" placeholder="Username..." />
                        </md-input-container>
                        <md-input-container class="md-block">
                            <label class="txtLabel">Password</label>
                            <input type="password" id="txtPassword" required name="password" ng-model="user.password" placeholder="Password..." />
                        </md-input-container>
                        <md-button ng-click="login(user)" class="md-raised md-primary" id="btnLogin" onkeypress="handleEnter(event)">&nbsp Login &nbsp</md-button>
                    </form>
                </md-card-content>
                <div class="txt-createUser">
                    <span id="txt-Signup" ng-click="register()">Click here to Sign Up</span>
                </div>
            </md-card>
        </md-content>
        <!-- Angular Material Dependencies-->

    </div>

    <!-- Angular Material requires Angular.js Libraries -->
    <script type="text/javascript" src="../Scripts/lib/angular.min.js"></script>
    <script type="text/javascript" src="../Scripts/lib/angular-animate.min.js"></script>
    <script type="text/javascript" src="../Scripts/lib/angular-aria.min.js"></script>
    <script type="text/javascript" src="../Scripts/lib/angular-messages.min.js"></script>

    <!-- Angular Material Library -->
    <script type="text/javascript" src="../Scripts/lib/angular-material.min.js"></script>

    <!--Script code-->
    <script type="text/javascript" src="../Scripts/Apps/LoginPageScript.js"></script>
    <script>
        var app = angular.module('LoginApps', ['ngMaterial', 'ngAnimate', 'ngAria', 'ngMessages']);
        var moveToViewPage = _spPageContextInfo.webAbsoluteUrl + "/Pages/ViewPage.aspx";
        app.controller('AppsControl', function ($scope, $mdDialog) {
            $scope.login = function (user) {
                var checkLogin = login(user.username, user.password);
                if (checkLogin == 0) {
                    $scope.showAlertInvalid();
                } else if (checkLogin == 1) {
                    window.location.href = moveToViewPage;
                } else if (checkLogin == 2) {
                    $scope.showAlertBanned();
                }
            }
            $scope.register = function () {
                window.location.href = _spPageContextInfo.webAbsoluteUrl + "/Pages/CreatePage.aspx";
            }

            $scope.status = '  ';
            $scope.customFullscreen = false;
            //show dialog invalid function
            $scope.showAlertInvalid = function (ev) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.body))
                        .clickOutsideToClose(true)
                        .title('Invalid')
                        .textContent('Invalid username or password. Please try again!')
                        .ariaLabel('Alert Dialog Demo')
                        .ok('Got it!')
                        .targetEvent(ev)
                );
            };

            //show dialog banned function
            $scope.showAlertBanned = function (ev) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.body))
                        .clickOutsideToClose(true)
                        .title('Banned')
                        .textContent('You has been banned. Call admin for more information')
                        .ariaLabel('Alert Dialog Demo')
                        .ok('Got it!')
                        .targetEvent(ev)
                );
            };
        });
    </script>
</asp:Content>
