﻿<%@ Page Language="C#" MasterPageFile="~masterurl/default.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <SharePoint:ScriptLink Name="sp.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />
    <link rel="Stylesheet" type="text/css" href="../Content/CreatePage.css" />
    <link rel="Stylesheet" type="text/css" href="../Content/angular-material.min.css" />

    <script type="text/javascript" src="../Scripts/lib/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../Scripts/lib/jquery.session.js"></script>


</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <md-content class="md-padding container">
    <div ng-app="addNewUser" ng-controller="ctrl" class="table-input">
        <table class="table table-striped">
            <tr>
                <td>
                    <label for="username">Username</label>
                </td>
                <td>
                    <input type="text" ng-model="user.username" id="username" ng-blur="checkUsername(user)" minlength="6" maxlength="32" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="password">Password</label>
                </td>
                <td>
                    <input type="password" ng-model="user.password" id="password" minlength="6" maxlength="32" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="cnfrmPwd">Confirm Password</label>
                </td>
                <td>
                    <input type="password" ng-model="user.confirmPassword" id="cnfrmPwd" minlength="6" maxlength="32" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="address">Address</label>
                </td>
                <td>
                    <input type="text" ng-model="user.address" id="address" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="description">Description</label>
                </td>
                <td>
                    <input type="text" ng-model="user.description" id="description" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="gender">Gender</label>
                </td>
                <td>
                    <select id="gender" ng-model="user.gender" ng-options="x.gender for x in genders"></select>
                </td>
            </tr>
        </table>
        <md-button class="md-raised btnLogin" id="btnAdd" type="button" ng-click="addNewUser(user)" ng-keydown="$event.keyCode === 13 && addNewUser(user)">ADD</md-button>
    </div>
    </md-content>



    <!-- Angular Material requires Angular.js Libraries -->
    <script type="text/javascript" src="../Scripts/lib/angular.min.js"></script>
    <script type="text/javascript" src="../Scripts/lib/angular-animate.min.js"></script>
    <script type="text/javascript" src="../Scripts/lib/angular-aria.min.js"></script>
    <script type="text/javascript" src="../Scripts/lib/angular-messages.min.js"></script>

    <!-- Angular Material Library -->
    <script type="text/javascript" src="../Scripts/lib/angular-material.min.js"></script>

    <!--Script code-->
    <script type="text/javascript" src="../Scripts/Apps/CreatePageScript.js"></script>

    <script>
        var addNewUserApp = angular.module('addNewUser', ['ngMaterial', 'ngAnimate', 'ngAria', 'ngMessages']);
        addNewUserApp.controller('ctrl', function ($scope) {
            $scope.genders = [
                { gender: "male", value: 1 },
                { gender: "female", value: 2 },
                { gender: "unknown", value: 0 }
            ];
            $scope.addNewUser = function (user) {
                var isValid = checkValid();
                if (isValid) {
                    var password = user.password;
                    var confirmPwd = user.confirmPassword;
                    if (password && confirmPwd && (password == confirmPwd)) {
                        var username = user.username;
                        var address = user.address;
                        var description = user.description;
                        var gender = user.gender;
                        addNewUser(username, password, address, description, gender);
                    } else {
                        showPopupDialog('Your password is not match');
                    }
                } else {
                    showPopupDialog('Some field is missing');
                }
            };
            $scope.checkUsername = function (user) {
                if (user.username) {
                    checkExist(user.username);
                }
            }
        });
    </script>
</asp:Content>
